<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>lchat</title>
		<style>
			html, body{
				margin:0;
				height: 100%;
			}
			#wrap{
				position: relative;
				width: 100%;
				height: 100%;
			}
			#contenido{
				position: absolute;
				border: 2px inset blue;
				overflow: auto;
				left: 10px; right: 10px; top: 10px; bottom: 40px;
			}
			#envio{
				position: absolute;
				bottom: 10px;
				left: 10px;
				right: 10px;
			}
			input{
				float:left;
			}
			input[name="usuario"]{
				width: 20%;
			}
			input[name="texto"]{
				width: 58%;
			}
			input[name="enviar"]{
				width: 18%;
				float:right;
			}
		</style>
		<script src="jquery-1.10.2.min.js"></script>
		<script>
			$(function() {
				$('#envio').submit(function(e){
					e.preventDefault();
					var usuario=$(this).find('input[name="usuario"]');
					var texto=$(this).find('input[name="texto"]');
					if(usuario.val()=='') alert('El Usuario no puede estar vacio!');
					else if(texto.val()=='') alert('El Texto no puede estar vacio!');
					else{
						$('#contenido').load('server.php',{
							peticion: 'insertar',
							usuario: usuario.val(),
							texto: texto.val()
						},function(){
							texto.val('').focus();
							$('#contenido').scrollTop($('#contenido').prop('scrollHeight'))
						});
					}
				});
				var recargar=function(){
					$('#contenido').load('server.php',{
						peticion: 'actualizar'
					});
				};
				recargar();
				setInterval(recargar, '2000');
			});
		</script>
    </head>
    <body>
		<div id="wrap">
			<div id="contenido"></div>
			<form id="envio">
				<input type="text" name="usuario" placeholder="Usuario"/>
				<input type="text" name="texto" placeholder="Texto"/>
				<input type="submit" name="enviar"/>
			</form>
		</div>
    </body>
</html>


